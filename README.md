# OpenProject Caprover Deployment

Clone this project for a Caprover Deployment of OpenProject

<p>
<img src="https://gitlab.com/pascal.cantaluppi/openproject/-/raw/master/img/banner.png" alt="CapRover" />
</p>

## Getting started

```bash
git clone https://gitlab.com/pascal.cantaluppi/openproject.git
cd openproject
npm i caprover -g
caprover login
caprover deploy
```

## Captain definition file

```json
{
  "schemaVersion": 2,
  "imageName": "openproject/community:11"
}
```

## Captain Configuration

### Port mapping

<p>
<img src="https://gitlab.com/pascal.cantaluppi/openproject/-/raw/master/img/port.png" alt="TCP Port" />
</p>

### Environmental variables

<p>
<img src="https://gitlab.com/pascal.cantaluppi/openproject/-/raw/master/img/env.png" alt="Env variables" />
</p>

### Persistent Directories

```bash
sudo mkdir -p /var/lib/openproject/{pgdata,assets}
```

<p>
<img src="https://gitlab.com/pascal.cantaluppi/openproject/-/raw/master/img/dir.png" alt="Directories" />
</p>
